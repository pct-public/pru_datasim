import matplotlib.pyplot as plt
import numpy as np
import random
import sys

#detector constants
NUM_LAYERS = 43
NUM_STAVES = 12 # per layer
NUM_CHIPS = 9 # per stave
NUM_PIXELS_WIDTH_CHIP = 1024
NUM_PIXELS_HEIGHT_CHIP = 512
MAX_WIDTH = NUM_PIXELS_WIDTH_CHIP*NUM_CHIPS
MAX_HEIGHT = NUM_PIXELS_HEIGHT_CHIP*NUM_STAVES

#data noise
MEAN = 0  #normal distribution starting value
SIGMA = 3 #normal distribution limit

DEVIATION = 600 # how many pixels the end point differs from start point in x and y direction. This is limit on random variable. not a constant in each track


def visualize_track(P, Q, local = True):
  """
  Used to vizualise any one track in a 3d space
  P = # start vector (point)
  Q = # end vector (point)
  local = See track in local space (zoom in on track), if set to false will display track in global perspective
  """

  d = Q-P #directinal vector (this vector will be perpendicular with the line that goes through P and Q)

  X_change = d[0]/(NUM_LAYERS) # the amount x changes for each z
  Y_change = d[1]/(NUM_LAYERS) # the amount y changes for each z


  x = [int(round(P[0] + (X_change*i))) for i in range(NUM_LAYERS)]
  x_= [P[0] + (X_change*i) for i in range(NUM_LAYERS)]
  y = [int(round(P[1] + (Y_change*i))) for i in range(NUM_LAYERS)]
  y_= [P[1] + (Y_change*i) for i in range(NUM_LAYERS)]
  z = [i for i in range(NUM_LAYERS)]

  fig = plt.figure()
  ax = fig.add_subplot(projection='3d')

  ax.scatter(x, z, y)
  ax.plot(x_, z, y_, color='r')
  ax.set_xlabel('X')
  ax.set_ylabel('Layer')
  ax.set_zlabel('Y')
  if(not local):
    ax.set_xlim(0,MAX_WIDTH) #horizontal
    ax.set_ylim(0,NUM_LAYERS) #depth
    ax.set_zlim(0,MAX_HEIGHT) #vertical
  plt.show()



def global_to_lscxy(x,y,z):
  """
  Global hit info to layer, stave, chip information

  This makes it easier to produces offsets that cross chip edges

  Returns layer, stave, chip, x, y
  """
  layer = 0
  stave = 0
  chip = 0
  x_ = 0
  y_ = 0

  layer = z
  stave = int(y/NUM_PIXELS_HEIGHT_CHIP) #round to floor value
  chip = int(x/NUM_PIXELS_WIDTH_CHIP) #round to floor value

  if layer % 2 == 0:
    if chip > 6:
      chip += 1
  else:
    if chip >= 2: #id 7 is never used
      chip = NUM_CHIPS - chip - 1
    else:
      chip = NUM_CHIPS - chip

  if layer % 2 == 0:
    if stave % 2 == 0:
      x_ = x % (NUM_PIXELS_WIDTH_CHIP)
    else:
      x_ = (NUM_PIXELS_WIDTH_CHIP - 1) - (x % (NUM_PIXELS_WIDTH_CHIP))
  else:
    total_pixels = (NUM_PIXELS_WIDTH_CHIP)*NUM_CHIPS - 1
    if stave % 2 == 0:
      x_ = (total_pixels - x) % (NUM_PIXELS_WIDTH_CHIP)
    else:
      x_ = (NUM_PIXELS_WIDTH_CHIP - 1) - ((total_pixels - x) % (NUM_PIXELS_WIDTH_CHIP))


  if stave % 2 == 0:
    y_ = y % NUM_PIXELS_HEIGHT_CHIP
  else:
    y_ = (NUM_PIXELS_HEIGHT_CHIP -1) - (y % NUM_PIXELS_HEIGHT_CHIP)



  assert chip != 7
  assert chip >= 0 and chip <= 9, "Expected >= 0 or <=9 was {} with x {}".format(chip, x)
  assert x_ < NUM_PIXELS_WIDTH_CHIP, "Expected <{}. Was {} layer {} stave {} chip {} x {} y {}".format(NUM_PIXELS_WIDTH_CHIP, x_, layer, stave,chip,x,y)
  assert y_ < NUM_PIXELS_HEIGHT_CHIP, "Expected <{}. Was {}".format(NUM_PIXELS_HEIGHT_CHIP, y_)
  assert stave >= 0 and stave < NUM_STAVES, "Was {} with y {}".format(stave, y)
  assert layer >= 0 and layer < NUM_LAYERS, "Was {}".format(layer)
  return layer, stave, chip, x_, y_

def create_hit(start_vector, end_vector):

  """
  Produces x,y,z hit for each layer based on start and end vector

  The output is global coordinates

  The output needs to be converted into layer, stave, chip innfo later
  """

  P = start_vector # start vector (point)
  Q = end_vector # end vector (point)
  d = Q-P #directinal vector (this vector will be perpendicular with the line that goes through P and Q)

  X_change = d[0]/(NUM_LAYERS) # the amount x changes for each z
  Y_change = d[1]/(NUM_LAYERS) # the amount y changes for each z


  x = [int(P[0] + (X_change*i)) for i in range(NUM_LAYERS)]
  y = [int(P[1] + (Y_change*i)) for i in range(NUM_LAYERS)]
  z = [i for i in range(NUM_LAYERS)]

  return(x,y,z)


def create_track(x, y, offsets):
  """
  x = start x position in global geometry
  y = start y position in global geometry
  layer = layer to apply offsets to
  offset_x = the amount of pixels to shift the x-axis
  offset_y = the amount of pixels to shift the y_axis
  offsets can be possitive and negative
  output list of all hits
  """
  output = []

  # start vector (point), static start of track(first hit)
  P = np.array([x,y,0])

  #Defines where the track should stop
  #randomly assign som value in x and y direction
  #random value should not be extreme +/- 100 pixels for now
  # must be within the frame of the detector

  x_dev = random.randrange(-DEVIATION, DEVIATION)
  y_dev = random.randrange(-DEVIATION, DEVIATION)
  #check if deviation is outside detector 
  # move inside if it is outside bounds
  if x + x_dev < 0:
    x_dev -= (x + x_dev)
  if x + x_dev > MAX_WIDTH:
    x_dev -= (x + x_dev) - MAX_WIDTH

  if y + y_dev < 0:
    y_dev -= (y + y_dev)
  if y + y_dev > MAX_HEIGHT:
    y_dev -= (y + y_dev) - MAX_HEIGHT

  Q = np.array([x + x_dev, y + y_dev,NUM_LAYERS-1]) # end vector (point)

  line = create_hit(P, Q)

  index = 0
  x_ = line[0]
  y_ = line[1]
  z_ = line[2]


  #insert normal distribution noise to the track
  x_new, y_new = apply_gaussian_distrib(x_, y_)

  #apply offsets before checking bounds(must be done before call to global_to_lscxy)
  insert_offset(x_new, y_new, offsets)

  #check that the gaussian distribution and offsets did not make the track invalid
  real_track = check_bounds(x_new, y_new) #checks if hit is outside detector bounds

  if real_track:
    for index in range(len(x_new)):
      output.append(global_to_lscxy(x_new[index], y_new[index], z_[index]))

  return output, real_track


def apply_gaussian_distrib(x, y):
  mean, sigma = MEAN, SIGMA # mean and standard deviation
  x_out, y_out = [], []
  #two values normal distrib
  #Don't want large outliers. Confine values within +/- sigma 

  for i in range(len(x)):

    s = np.random.normal(mean, sigma, 1)

    x_dev = int(round(s[0]))
    x_out.append(x[i] + x_dev)

  for i in range(len(y)):

    s = np.random.normal(mean, sigma, 1)

    y_dev = int(round(s[0]))
    y_out.append(y[i] + y_dev)

  return x_out, y_out

def check_bounds(x, y):
  """
  With the global coordinates of x and y check if it is within the bounds of the detector
  """
  for v in x:
    if v < 0: #cant have pixel value less than 0
      return False
    if v > MAX_WIDTH - 1:
      return False

  for v in y:
    if v < 0:
      return False
    if v > MAX_HEIGHT - 1:
      return False

  return True


def insert_offset(x, y, offsets):
  """
  Insert a offset into the given layer

  x, y is list of hits in track

  offsets is dictionary of offsets that should be added.
  Layer:(offset_x, offset_y)
  """
  assert len(x) == len(y), "Invalid track"

  for i in range(len(x)):
    if i in offsets.keys(): #check if layer is in list of layers that should be shifted
      #apply the offset if layer should be shifted
      x[i] += offsets[i][0]
      y[i] += offsets[i][1]




def simulate_random_tracks(n = 100, offsets = {}):
  """
  n = number of random tracks

  layer = layer to apply offsets to
  offset_x = the amount of pixels to shift the x-axis
  offset_y = the amount of pixels to shift the y_axis

  offsets can be possitive and negative
  """
  print("Simulating", n, "tracks")

  with open("simData.csv", "w") as file:
    file.write("Layer,Stave,Chip,X,Y,eventID\n")

    for i in range(n):
      #random start at any pixel on first layer
      real = False
      while not real:
        x = random.randrange(5, MAX_WIDTH - 1)
        y = random.randrange(5, MAX_HEIGHT - 1)
        track = create_track(x, y, offsets)
        real = track[1]

      for e in track[0]:
        s = ', '.join(map(str, e))
        file.write(s+","+str(i)+'\n')





"""
Defining a list of layers that should be offset, and by how much.

Key: Layer
Value: (offset_x, offset_y)

"""

offsets_ = {
#  6:(-10,6),
#  9:(4,-7),
#  10:(-3,2),
#  11:(10,2),
#  15:(0,-9),
#  22:(-8,-3),
#  23:(8,3),
#  30:(6,5),
#  35:(-7,-5),
#  40:(2,8)
}


#NOTE: that tracks leaving the sensor is considered invalid and discarded.
#I do not know if this is considered realistic. This should be confirmed with someone who knows.

if len(sys.argv) > 1: #if an argument has been given simulate "argument" numbers of tracks
  simulate_random_tracks(int(sys.argv[1]), offsets=offsets_)
else: #base case of 100 tracks
  simulate_random_tracks(offsets = offsets_)


