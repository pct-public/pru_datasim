# Alignment simulation data generator

This module generates straight path particle hit data that can be used for alignment testing or any other situation requiring this kind of data.

The simulated data is for now only single pixel hits(cluster centroids) and does not simulate cluster hits.
## How to use

See google for how to do it in your particular OS(the following is for Ubuntu).

1. Clone the repository.
2. Inside the `simulation` directory
   - Requires: matplotlib, numpy
```Console
 > python3 simulate_data.py n
```
Where `n` is number of tracks you want.

3. You should now see a file called simData.csv in the same directory as `simulate_data.py`


Script does not do any error checking so weird errors could happen at e.g file creation or writing.


To include offsets see the `offsets_` dictionary at bottom of script. Add offsets with key=layer, value=(x,y) e.g 10:(13, -2). More examples given in 
`offsets_`
dictionary. Defaults to no misaligned elements.
## Data structure

The simulated data is stored in a .csv file using the structure:

Layer, Stave, Chip, X, Y, eventID

eventID groups every hit in a track e.g eventID = 1 is every hit belonging to track 1


