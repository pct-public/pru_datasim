# pRU Datasim
A tool to convert geant4 simulation data into pRU data format in converter folder. 

Includes tool to simulate straight path particles in simulation folder.

## Requirements
Import numpy 1.16.2
Root file containing hit information in the following manner:
```
posX [-135, 135] mm,
posY [-67.5, 67.5] mm,
posZ [0, 175] mm, 
eventID [0,650 k]
clockTime [0, 6.5k * (10 µs / 25 ns)]
```
## Generating pRU data
In order to genereate a pRU data format file the following steps must be taken:
```
root -l
.x readPctFiles.C ("name.root") 
```
(Make sure you have a dat folder created)
This generates hit information which is stored into row_col files that include information about the chip id, stave id, readout unit and bunchcounter.
```
python3.4 test_data_chain.py
```
Creates pRU data which is stored in data_chain/data_chain_pruformat.txt


# Generating .root format
Clone datasim branch from data-format-sw (4fb2780f)
```
cd bin
mkdir output
cd build
cmake ..
make lib
```
Clone dev branch from alpide-data-sw (c20f3064)
Copy libEventDict_rdict.pcm from data-format-sw/build to alpide-sw/build/bin
```
cd build
make root_file
cd bin
./root_file PATH/TO/data_chain_pruformat.txt
```

Creates Event_i.root file containing hit information in the following manner:
```
chip ID
stave ID
readout unit ID
std::<pair<vector>> xy
bunch counter
abs time
```

# Running simulation

See README in the simulation folder.


