from alpide_data import *
from pru_data import *
import os
import shutil

# Test case 0
# Purpose: To check alpide_data event handling, and that byte counter is correct when clustering is off
# Test the byte counter with 100 events with incrementing size, no clustering of pixels
# A good chunch of leading and trailing commas

print("Generating testcase 0")
testcase = "testcase0"
shutil.rmtree(testcase, ignore_errors=True)
os.makedirs(testcase, exist_ok=True)

first_iter = True
print("Generating ALPIDE data")
for i in range(1, 150):
    data_gen = Alpide_Data_Generator()
    data = data_gen.static_data(0, 0, 0, i-1)
    data_gen.write_row_col_csv(data, "{}/row_col_{}.txt".format(testcase, i), force=True)

    frame = Alpide_Frame_Generator()
    frame.read_row_col_list("{}/row_col_{}.txt".format(testcase, i))
    frame.encode_reg_enc_addr()
    frame.encode_alpide_df(reg_enc_addr_list=None, add_random_comma=False, add_random_idle=False, clustering_enable=False)
    if first_iter:
        num_leading_comma = 300
    else:
        num_leading_comma = 0
    frame.encode_8b10b(alpide_frame=None, num_leading_comma=num_leading_comma, num_trailing_comma=10)

    frame.write_alpide_format_csv("{}/alpide_format_{}.txt".format(testcase, i), force=True)
    frame.write_8b10b_csv("{0}/{0}_alpideformat_8b10b.txt".format(testcase), force=True, append=True)
    first_iter = False

print("Generating pRU data")
for i in range(1, 150):
    frame = PRU_Frame_Generator()
    frame.alpide_frame.read_alpide_frame("{}/alpide_format_{}.txt".format(testcase, i))
    frame.encode_pru_frame(frame_id=i-1)
    frame.write_pru_format_csv("{0}/{0}_pruformat.txt".format(testcase), force=True, append=True)


# Test case 1
# Check that byte count is correct when clustering is on

print("Generating testcase 1")
testcase = "testcase1"
shutil.rmtree(testcase, ignore_errors=True)
os.makedirs(testcase, exist_ok=True)

first_iter = True
print("Generating ALPIDE data")
for i in range(1, 150):
    data_gen = Alpide_Data_Generator()
    data = data_gen.static_data(0, 0, 0, i-1)
    data_gen.write_row_col_csv(data, "{}/row_col_{}.txt".format(testcase, i), force=True)

    frame = Alpide_Frame_Generator()
    frame.read_row_col_list("{}/row_col_{}.txt".format(testcase, i))
    frame.encode_reg_enc_addr()
    frame.encode_alpide_df(reg_enc_addr_list=None, add_random_idle=False, clustering_enable=True)
    if first_iter:
        num_leading_comma = 300
    else:
        num_leading_comma = 0
    frame.encode_8b10b(alpide_frame=None, num_leading_comma=num_leading_comma, num_trailing_comma=10)

    frame.write_alpide_format_csv("{}/alpide_format_{}.txt".format(testcase, i), force=True)
    frame.write_8b10b_csv("{0}/{0}_alpideformat_8b10b.txt".format(testcase), force=True, append=True)
    first_iter = False

print("Generating pRU data")
for i in range(1, 150):
    frame = PRU_Frame_Generator()
    frame.alpide_frame.read_alpide_frame("{}/alpide_format_{}.txt".format(testcase, i))
    frame.encode_pru_frame(frame_id=i-1)
    frame.write_pru_format_csv("{0}/{0}_pruformat.txt".format(testcase), force=True, append=True)

# Test case 2
# Check back-to-back data

print("Generating testcase 2")
testcase = "testcase2"
shutil.rmtree(testcase, ignore_errors=True)
os.makedirs(testcase, exist_ok=True)

first_iter = True
print("Generating ALPIDE data")
for i in range(1, 150):
    data_gen = Alpide_Data_Generator()
    data = data_gen.static_data(0, 0, 0, i-1)
    data_gen.write_row_col_csv(data, "{}/row_col_{}.txt".format(testcase, i), force=True)

    frame = Alpide_Frame_Generator()
    frame.read_row_col_list("{}/row_col_{}.txt".format(testcase, i))
    frame.encode_reg_enc_addr()
    frame.encode_alpide_df(reg_enc_addr_list=None, add_random_comma=False, add_random_idle=False, clustering_enable=True)
    if first_iter:
        num_leading_comma = 300
        first_iter = False
    else:
        num_leading_comma = 0
    frame.encode_8b10b(alpide_frame=None, num_leading_comma=num_leading_comma, num_trailing_comma=0)

    frame.write_alpide_format_csv("{}/alpide_format_{}.txt".format(testcase, i), force=True)
    frame.write_8b10b_csv("{0}/{0}_alpideformat_8b10b.txt".format(testcase), force=True, append=True)
    

print("Generating pRU data")
for i in range(1, 150):
    frame = PRU_Frame_Generator()
    frame.alpide_frame.read_alpide_frame("{}/alpide_format_{}.txt".format(testcase, i))
    frame.encode_pru_frame(frame_id=i-1)
    frame.write_pru_format_csv("{0}/{0}_pruformat.txt".format(testcase), force=True, append=True)

# Test case 3
# Check ALPIDE trailer flags
print("Generating testcase 3")
testcase = "testcase3"
shutil.rmtree(testcase, ignore_errors=True)
os.makedirs(testcase, exist_ok=True)

flags = ['busy_violation', 'flushed_incomplete', 'strobe_extended', 'busy_transition', 'data_overrun', 'fatal_condition', 
         'flushed_strobe', 'flushed_busy', 'strobe_busy', 'flushed_strobe_busy']

print("Generating ALPIDE data")
first_iter = True
for flag in flags:
    data_gen = Alpide_Data_Generator()
    data = data_gen.random_data(0.05)
    data_gen.write_row_col_csv(data, "{}/row_col_{}.txt".format(testcase, flag), force=True)

    frame = Alpide_Frame_Generator()
    frame.read_row_col_list("{}/row_col_{}.txt".format(testcase, flag))
    frame.encode_reg_enc_addr()
    if flag == 'busy_violation':
        frame.encode_alpide_df(add_random_comma=False, add_random_idle=False, clustering_enable=True, busy_violation=True)
    elif flag == 'flushed_incomplete':
        frame.encode_alpide_df(add_random_comma=False, add_random_idle=False, clustering_enable=True, flushed_incomplete=True)
    elif flag == 'strobe_extended':
        frame.encode_alpide_df(add_random_comma=False, add_random_idle=False, clustering_enable=True, strobe_extended=True)
    elif flag == 'busy_transition':
        frame.encode_alpide_df(add_random_comma=False, add_random_idle=False, clustering_enable=True, busy_transition=True)
    elif flag == 'data_overrun':
        frame.encode_alpide_df(add_random_comma=False, add_random_idle=False, clustering_enable=True, data_overrun=True)
    elif flag == 'fatal_condition':
        frame.encode_alpide_df(add_random_comma=False, add_random_idle=False, clustering_enable=True, fatal_condition=True)
    elif flag == 'flushed_strobe':
        frame.encode_alpide_df(add_random_comma=False, add_random_idle=False, clustering_enable=True, flushed_incomplete=True, strobe_extended=True)
    elif flag == 'flushed_busy':
        frame.encode_alpide_df(add_random_comma=False, add_random_idle=False, clustering_enable=True, flushed_incomplete=True, busy_transition=True)
    elif flag == 'strobe_busy':
        frame.encode_alpide_df(add_random_comma=False, add_random_idle=False, clustering_enable=True, strobe_extended=True, busy_transition=True)
    elif flag == 'flushed_strobe_busy':
        frame.encode_alpide_df(add_random_comma=False, add_random_idle=False, clustering_enable=True, flushed_incomplete=True, strobe_extended=True, busy_transition=True)
    if first_iter:
        num_leading_comma = 300
        first_iter = False
    else:
        num_leading_comma = 0
    frame.encode_8b10b(alpide_frame=None, num_leading_comma=num_leading_comma, num_trailing_comma=10)
    frame.write_alpide_format_csv("{}/alpide_format_{}.txt".format(testcase, flag), force=True)
    frame.write_8b10b_csv("{0}/{0}_alpideformat_8b10b.txt".format(testcase), force=True, append=True)
    

print("Generating pRU data")
for i, flag in enumerate(flags):
    frame = PRU_Frame_Generator()
    frame.alpide_frame.read_alpide_frame("{}/alpide_format_{}.txt".format(testcase, flag))
    frame.encode_pru_frame(frame_id=i)
    frame.write_pru_format_csv("{0}/{0}_pruformat.txt".format(testcase), force=True, append=True)


# Test case 4
# Check random idle and comma with filtering
print("Generating testcase 4")
testcase = "testcase4"
shutil.rmtree(testcase, ignore_errors=True)
os.makedirs(testcase, exist_ok=True)

cases = ['idles', 'commas', 'both']

print("Generating ALPIDE data")
first_iter = True
for case in cases:
    data_gen = Alpide_Data_Generator()
    data = data_gen.random_data(0.05)
    data_gen.write_row_col_csv(data, "{}/row_col_{}.txt".format(testcase, case), force=True)

    frame = Alpide_Frame_Generator()
    frame.read_row_col_list("{}/row_col_{}.txt".format(testcase, case))
    frame.encode_reg_enc_addr()
    if case == 'idles':
        frame.encode_alpide_df(add_random_comma=False, add_random_idle=True, clustering_enable=True)
    elif case == 'commas':
        frame.encode_alpide_df(add_random_comma=True, add_random_idle=False, clustering_enable=True)
    elif case == 'both':
        frame.encode_alpide_df(add_random_comma=True, add_random_idle=True, clustering_enable=True)

    if first_iter:
        num_leading_comma = 300
        first_iter = False
    else:
        num_leading_comma = 0
    frame.encode_8b10b(alpide_frame=None, num_leading_comma=num_leading_comma, num_trailing_comma=10)
    frame.write_alpide_format_csv("{}/alpide_format_{}.txt".format(testcase, case), force=True)
    frame.write_8b10b_csv("{0}/{0}_alpideformat_8b10b.txt".format(testcase), force=True, append=True)
    

print("Generating pRU data")
for i, case in enumerate(cases):
    frame = PRU_Frame_Generator()
    frame.alpide_frame.read_alpide_frame("{}/alpide_format_{}.txt".format(testcase, case))
    frame.encode_pru_frame(frame_id=i, filter_comma=True, filter_idle=True)
    frame.write_pru_format_csv("{0}/{0}_pruformat.txt".format(testcase), force=True, append=True)

# Test case 5
# Check random idle and comma with no filtering
print("Generating testcase 5")
testcase = "testcase5"
shutil.rmtree(testcase, ignore_errors=True)
os.makedirs(testcase, exist_ok=True)

cases = ['idles', 'commas', 'both']

print("Generating ALPIDE data")
first_iter = True
for case in cases:
    data_gen = Alpide_Data_Generator()
    data = data_gen.random_data(0.05)
    data_gen.write_row_col_csv(data, "{}/row_col_{}.txt".format(testcase, case), force=True)

    frame = Alpide_Frame_Generator()
    frame.read_row_col_list("{}/row_col_{}.txt".format(testcase, case))
    frame.encode_reg_enc_addr()
    if case == 'idles':
        frame.encode_alpide_df(add_random_comma=False, add_random_idle=True, clustering_enable=True)
    elif case == 'commas':
        frame.encode_alpide_df(add_random_comma=True, add_random_idle=False, clustering_enable=True)
    elif case == 'both':
        frame.encode_alpide_df(add_random_comma=True, add_random_idle=True, clustering_enable=True)

    if first_iter:
        num_leading_comma = 300
        first_iter = False
    else:
        num_leading_comma = 0
    frame.encode_8b10b(alpide_frame=None, num_leading_comma=num_leading_comma, num_trailing_comma=10)
    frame.write_alpide_format_csv("{}/alpide_format_{}.txt".format(testcase, case), force=True)
    frame.write_8b10b_csv("{0}/{0}_alpideformat_8b10b.txt".format(testcase), force=True, append=True)
    

print("Generating pRU data")
for i, case in enumerate(cases):
    frame = PRU_Frame_Generator()
    frame.alpide_frame.read_alpide_frame("{}/alpide_format_{}.txt".format(testcase, case))
    frame.encode_pru_frame(frame_id=i, filter_comma=False, filter_idle=False)
    frame.write_pru_format_csv("{0}/{0}_pruformat.txt".format(testcase), force=True, append=True)


# Test case 6
# Check wrong chip id with check_id on

print("Generating testcase 6")
testcase = "testcase6"
shutil.rmtree(testcase, ignore_errors=True)
os.makedirs(testcase, exist_ok=True)

first_iter = True
print("Generating ALPIDE data")
for i in range(1, 11):
    data_gen = Alpide_Data_Generator()
    data = data_gen.random_data(0.5)
    data_gen.write_row_col_csv(data, "{}/row_col_{}.txt".format(testcase, i), force=True)

    frame = Alpide_Frame_Generator()
    frame.read_row_col_list("{}/row_col_{}.txt".format(testcase, i))
    frame.encode_reg_enc_addr()
    frame.encode_alpide_df(add_random_comma=True, add_random_idle=True, clustering_enable=True, chipid=1)
    if first_iter:
        num_leading_comma = 300
    else:
        num_leading_comma = 0
    frame.encode_8b10b(alpide_frame=None, num_leading_comma=num_leading_comma, num_trailing_comma=1)

    frame.write_alpide_format_csv("{}/alpide_format_{}.txt".format(testcase, i), force=True)
    frame.write_8b10b_csv("{0}/{0}_alpideformat_8b10b.txt".format(testcase), force=True, append=True)
    first_iter = False

# Test case 7
# Check wrong chip id with check_id off

print("Generating testcase 7")
testcase = "testcase7"
shutil.rmtree(testcase, ignore_errors=True)
os.makedirs(testcase, exist_ok=True)

first_iter = True
print("Generating ALPIDE data")
for i in range(1, 11):
    data_gen = Alpide_Data_Generator()
    data = data_gen.random_data(0.01)
    data_gen.write_row_col_csv(data, "{}/row_col_{}.txt".format(testcase, i), force=True)

    frame = Alpide_Frame_Generator()
    frame.read_row_col_list("{}/row_col_{}.txt".format(testcase, i))
    frame.encode_reg_enc_addr()
    frame.encode_alpide_df(add_random_comma=True, add_random_idle=True, clustering_enable=True, chipid=1)
    if first_iter:
        num_leading_comma = 300
    else:
        num_leading_comma = 0
    frame.encode_8b10b(alpide_frame=None, num_leading_comma=num_leading_comma, num_trailing_comma=1)

    frame.write_alpide_format_csv("{}/alpide_format_{}.txt".format(testcase, i), force=True)
    frame.write_8b10b_csv("{0}/{0}_alpideformat_8b10b.txt".format(testcase), force=True, append=True)
    first_iter = False

print("Generating pRU data")
for i in range(1, 11):
    frame = PRU_Frame_Generator()
    frame.alpide_frame.read_alpide_frame("{}/alpide_format_{}.txt".format(testcase, i))
    frame.encode_pru_frame(frame_id=i-1)
    frame.write_pru_format_csv("{0}/{0}_pruformat.txt".format(testcase), force=True, append=True)


# Test case 8
# Random Empty frames without compression

print("Generating testcase 8")
testcase = "testcase8"
shutil.rmtree(testcase, ignore_errors=True)
os.makedirs(testcase, exist_ok=True)

first_iter = True
print("Generating ALPIDE data")

for i in range(1, 101):
    data_gen = Alpide_Data_Generator()
    data = data_gen.random_data(0.001)
    data_gen.write_row_col_csv(data, "{}/row_col_{}.txt".format(testcase, i), force=True)

    frame = Alpide_Frame_Generator()
    bunch_cnt = random.randint(0, 0xFF)
    if first_iter or bool(random.getrandbits(1)):
        frame.read_row_col_list("{}/row_col_{}.txt".format(testcase, i))
        frame.encode_reg_enc_addr()
        frame.encode_alpide_df(add_random_comma=True, add_random_idle=True, clustering_enable=True, bunch_cnt=bunch_cnt)
        if first_iter:
            num_leading_comma = 300
        else:
            num_leading_comma = 0
    else:
        frame.alpide_frame.append(frame.empty_frame(0, bunch_cnt))
    frame.encode_8b10b(alpide_frame=None, num_leading_comma=num_leading_comma, num_trailing_comma=1)

    frame.write_alpide_format_csv("{}/alpide_format.txt".format(testcase), force=True, append=True)
    frame.write_8b10b_csv("{0}/{0}_alpideformat_8b10b.txt".format(testcase), force=True, append=True)
    first_iter = False

print("Generating pRU data")
frame = PRU_Frame_Generator()
frame.alpide_frame.read_alpide_frame("{}/alpide_format.txt".format(testcase))
frame.encode_multiple_pru_frames(starting_frame_id=0, empty_frame_compression=1)
frame.write_pru_format_csv("{0}/{0}_pruformat.txt".format(testcase), force=True, append=True)

# Test case 9
# Random Empty frames with full compression

print("Generating testcase 9")
testcase = "testcase9"
shutil.rmtree(testcase, ignore_errors=True)
os.makedirs(testcase, exist_ok=True)

first_iter = True
print("Generating ALPIDE data")

for i in range(1, 101):
    data_gen = Alpide_Data_Generator()
    data = data_gen.random_data(0.001)
    data_gen.write_row_col_csv(data, "{}/row_col_{}.txt".format(testcase, i), force=True)

    frame = Alpide_Frame_Generator()
    bunch_cnt = random.randint(0, 0xFF)
    if first_iter or bool(random.getrandbits(1)):
        frame.read_row_col_list("{}/row_col_{}.txt".format(testcase, i))
        frame.encode_reg_enc_addr()
        frame.encode_alpide_df(add_random_comma=True, add_random_idle=True, clustering_enable=True, bunch_cnt=bunch_cnt)
        if first_iter:
            num_leading_comma = 300
        else:
            num_leading_comma = 0
    else:
        frame.alpide_frame.append(frame.empty_frame(0, bunch_cnt))
    frame.encode_8b10b(alpide_frame=None, num_leading_comma=num_leading_comma, num_trailing_comma=1)

    frame.write_alpide_format_csv("{}/alpide_format.txt".format(testcase), force=True, append=True)
    frame.write_8b10b_csv("{0}/{0}_alpideformat_8b10b.txt".format(testcase), force=True, append=True)
    first_iter = False

print("Generating pRU data")
frame = PRU_Frame_Generator()
frame.alpide_frame.read_alpide_frame("{}/alpide_format.txt".format(testcase))
frame.encode_multiple_pru_frames(starting_frame_id=0, empty_frame_compression=0)
frame.write_pru_format_csv("{0}/{0}_pruformat.txt".format(testcase), force=True, append=True)

# Test case 10
# Random Empty frames with some compression

print("Generating testcase 10")
testcase = "testcase10"
shutil.rmtree(testcase, ignore_errors=True)
os.makedirs(testcase, exist_ok=True)

first_iter = True
print("Generating ALPIDE data")

for i in range(1, 101):
    data_gen = Alpide_Data_Generator()
    data = data_gen.random_data(0.001)
    data_gen.write_row_col_csv(data, "{}/row_col_{}.txt".format(testcase, i), force=True)

    frame = Alpide_Frame_Generator()
    bunch_cnt = random.randint(0, 0xFF)
    if first_iter or bool(random.getrandbits(1)):
        frame.read_row_col_list("{}/row_col_{}.txt".format(testcase, i))
        frame.encode_reg_enc_addr()
        frame.encode_alpide_df(add_random_comma=True, add_random_idle=True, clustering_enable=True, bunch_cnt=bunch_cnt)
        if first_iter:
            num_leading_comma = 300
        else:
            num_leading_comma = 0
    else:
        frame.alpide_frame.append(frame.empty_frame(0, bunch_cnt))
    frame.encode_8b10b(alpide_frame=None, num_leading_comma=num_leading_comma, num_trailing_comma=1)

    frame.write_alpide_format_csv("{}/alpide_format.txt".format(testcase), force=True, append=True)
    frame.write_8b10b_csv("{0}/{0}_alpideformat_8b10b.txt".format(testcase), force=True, append=True)
    first_iter = False

print("Generating pRU data")
frame = PRU_Frame_Generator()
frame.alpide_frame.read_alpide_frame("{}/alpide_format.txt".format(testcase))
frame.encode_multiple_pru_frames(starting_frame_id=0, empty_frame_compression=3)
frame.write_pru_format_csv("{0}/{0}_pruformat.txt".format(testcase), force=True, append=True)

# Test case 11
# Random frames (normal and empty) with random IDs with check ID on, also set values on the other fields Random Empty frames with some compression

print("Generating testcase 11")
testcase = "testcase11"
shutil.rmtree(testcase, ignore_errors=True)
os.makedirs(testcase, exist_ok=True)

first_iter = True
print("Generating ALPIDE data")

spill_id = 0xF01
trig_source = 0x2
mode = 0x1
abs_time = 0xFFAA3

data_gen = Alpide_Data_Generator()
data = data_gen.random_data(0.001)
data_gen.write_row_col_csv(data, "{}/row_col.txt".format(testcase), force=True)

for i in range(1, 3000):
    frame = Alpide_Frame_Generator()
    bunch_cnt = random.randint(0, 0xFF)
    chipid = random.randint(0, 14)
    if first_iter or bool(random.getrandbits(1)):
        frame.read_row_col_list("{}/row_col.txt".format(testcase))
        frame.encode_reg_enc_addr()
        frame.encode_alpide_df(add_random_comma=True, add_random_idle=True, clustering_enable=True, bunch_cnt=bunch_cnt, chipid=chipid)
        if first_iter:
            num_leading_comma = 300
        else:
            num_leading_comma = 0
    else:
        frame.alpide_frame.append(frame.empty_frame(chipid, bunch_cnt))
    num_trailing_comma = random.randint(1, 5)
    frame.encode_8b10b(alpide_frame=None, num_leading_comma=num_leading_comma, num_trailing_comma=num_trailing_comma)

    frame.write_alpide_format_csv("{}/alpide_format.txt".format(testcase), force=True, append=True)
    frame.write_8b10b_csv("{0}/{0}_alpideformat_8b10b.txt".format(testcase), force=True, append=True)
    first_iter = False

print("Generating pRU data")
frame = PRU_Frame_Generator()
frame.alpide_frame.read_alpide_frame("{}/alpide_format.txt".format(testcase))
frame.encode_multiple_pru_frames(starting_frame_id=0, empty_frame_compression=3, ru=30, stave=12, chipid=5, spill_id=spill_id, trig_source=trig_source, mode=mode, abs_time=abs_time, check_id=True)
frame.write_pru_format_csv("{0}/{0}_pruformat.txt".format(testcase), force=True, append=True)


# Test case 12
# Max Size and Max Wait Time set to zero i.e. no checking

print("Generating testcase 12")
testcase = "testcase12"
shutil.rmtree(testcase, ignore_errors=True)
os.makedirs(testcase, exist_ok=True)

first_iter = True
print("Generating ALPIDE data")
for i in range(1, 11):
    data_gen = Alpide_Data_Generator()
    data = data_gen.random_data(0.01)
    data_gen.write_row_col_csv(data, "{}/row_col_{}.txt".format(testcase, i), force=True)

    frame = Alpide_Frame_Generator()
    frame.read_row_col_list("{}/row_col_{}.txt".format(testcase, i))
    frame.encode_reg_enc_addr()
    frame.encode_alpide_df(add_random_comma=True, add_random_idle=True, clustering_enable=True)
    if first_iter:
        num_leading_comma = 300
    else:
        num_leading_comma = 0
    frame.encode_8b10b(alpide_frame=None, num_leading_comma=num_leading_comma, num_trailing_comma=0)

    frame.write_alpide_format_csv("{}/alpide_format_{}.txt".format(testcase, i), force=True)
    frame.write_8b10b_csv("{0}/{0}_alpideformat_8b10b.txt".format(testcase), force=True, append=True)
    first_iter = False

print("Generating pRU data")
for i in range(1, 11):
    frame = PRU_Frame_Generator()
    frame.alpide_frame.read_alpide_frame("{}/alpide_format_{}.txt".format(testcase, i))
    frame.encode_pru_frame(frame_id=i-1)
    frame.write_pru_format_csv("{0}/{0}_pruformat.txt".format(testcase), force=True, append=True)

# Test case 13
# Max Size Error

print("Generating testcase 13")
testcase = "testcase13"
shutil.rmtree(testcase, ignore_errors=True)
os.makedirs(testcase, exist_ok=True)

first_iter = True
print("Generating ALPIDE data")
for i in range(1, 51):
    data_gen = Alpide_Data_Generator()
    data = data_gen.random_data(0.1)
    data_gen.write_row_col_csv(data, "{}/row_col_{}.txt".format(testcase, i), force=True)

    frame = Alpide_Frame_Generator()
    frame.read_row_col_list("{}/row_col_{}.txt".format(testcase, i))
    frame.encode_reg_enc_addr()
    frame.encode_alpide_df(add_random_comma=True, add_random_idle=True, clustering_enable=True, chipid=0)
    if first_iter:
        num_leading_comma = 300
    else:
        num_leading_comma = 0
    num_trailing_comma = random.randint(0, 10)
    frame.encode_8b10b(alpide_frame=None, num_leading_comma=num_leading_comma, num_trailing_comma=num_trailing_comma)

    frame.write_alpide_format_csv("{}/alpide_format_{}.txt".format(testcase, i), force=True)
    frame.write_8b10b_csv("{0}/{0}_alpideformat_8b10b.txt".format(testcase), force=True, append=True)
    first_iter = False

print("Generating pRU data")
for i in range(1, 51):
    frame = PRU_Frame_Generator()
    frame.alpide_frame.read_alpide_frame("{}/alpide_format_{}.txt".format(testcase, i))
    frame.encode_pru_frame(frame_id=i-1, max_size=30)
    frame.write_pru_format_csv("{0}/{0}_pruformat.txt".format(testcase), force=True, append=True)

# Test case 14
# Max Wait Time Error

print("Generating testcase 14")
testcase = "testcase14"
shutil.rmtree(testcase, ignore_errors=True)
os.makedirs(testcase, exist_ok=True)

first_iter = True
print("Generating ALPIDE data")
for i in range(1, 51):
    data_gen = Alpide_Data_Generator()
    data = data_gen.random_data(0.01)
    data_gen.write_row_col_csv(data, "{}/row_col_{}.txt".format(testcase, i), force=True)

    frame = Alpide_Frame_Generator()
    frame.read_row_col_list("{}/row_col_{}.txt".format(testcase, i))
    frame.encode_reg_enc_addr()
    frame.encode_alpide_df(add_random_comma=True, add_random_idle=True, clustering_enable=True, chipid=0xA)
    if first_iter:
        num_leading_comma = 300
    else:
        num_leading_comma = 0
    num_trailing_comma = random.randint(0, 10)
    frame.encode_8b10b(alpide_frame=None, num_leading_comma=num_leading_comma, num_trailing_comma=num_trailing_comma)

    frame.write_alpide_format_csv("{}/alpide_format_{}.txt".format(testcase, i), force=True)
    frame.write_8b10b_csv("{0}/{0}_alpideformat_8b10b.txt".format(testcase), force=True, append=True)
    first_iter = False

print("Generating pRU data")
for i in range(1, 51):
    frame = PRU_Frame_Generator()
    frame.alpide_frame.read_alpide_frame("{}/alpide_format_{}.txt".format(testcase, i))
    frame.encode_pru_frame(frame_id=i-1, max_wait_time=3, chipid=0xA)
    frame.write_pru_format_csv("{0}/{0}_pruformat.txt".format(testcase), force=True, append=True)

# Test case 15
# Random busys

print("Generating testcase 15")
testcase = "testcase15"
shutil.rmtree(testcase, ignore_errors=True)
os.makedirs(testcase, exist_ok=True)

first_iter = True
print("Generating ALPIDE data")
for i in range(1, 151):
    data_gen = Alpide_Data_Generator()
    data = data_gen.random_data(0.005)
    data_gen.write_row_col_csv(data, "{}/row_col_{}.txt".format(testcase, i), force=True)

    frame = Alpide_Frame_Generator()
    frame.read_row_col_list("{}/row_col_{}.txt".format(testcase, i))
    frame.encode_reg_enc_addr()
    bunch_cnt = random.randint(0, 0xFF)
    frame.encode_alpide_df(add_random_comma=False, add_random_idle=False, add_random_busy=True, clustering_enable=True, bunch_cnt=bunch_cnt, chipid=0xE)
    if first_iter:
        num_leading_comma = 300
    else:
        num_leading_comma = 0
    num_trailing_comma = random.randint(0, 10)
    frame.encode_8b10b(alpide_frame=None, num_leading_comma=num_leading_comma, num_trailing_comma=num_trailing_comma)

    frame.write_alpide_format_csv("{}/alpide_format.txt".format(testcase), force=True, append=True)
    frame.write_8b10b_csv("{0}/{0}_alpideformat_8b10b.txt".format(testcase), force=True, append=True)
    first_iter = False

print("Generating pRU data")
frame = PRU_Frame_Generator()
frame.alpide_frame.read_alpide_frame("{}/alpide_format.txt".format(testcase))
frame.encode_multiple_pru_frames(starting_frame_id=0, chipid=0xE)
frame.write_pru_format_csv("{0}/{0}_pruformat.txt".format(testcase), force=True, append=True) 


# Test case 16
# Empty Region

print("Generating testcase 16")
testcase = "testcase16"
shutil.rmtree(testcase, ignore_errors=True)
os.makedirs(testcase, exist_ok=True)

print("Generating ALPIDE data")

# Manual Data

chipid = 0xF

frame = Alpide_Frame_Generator()
bunch_cnt = random.randint(0xF, 0xFF)
frame.alpide_frame.append(frame.header(chipid, bunch_cnt))
frame.alpide_frame.extend(frame.random_idle_comma(True, True, False))
frame.alpide_frame.append(frame.region_header(4))
frame.alpide_frame.extend(frame.random_idle_comma(True, True, False))
frame.alpide_frame.append(frame.region_header(5))
frame.alpide_frame.append(frame.data_long(4, 4 , 4))
frame.alpide_frame.extend(frame.random_idle_comma(True, True, False))
frame.alpide_frame.append(frame.trailer())

num_leading_comma = 300
num_trailing_comma = 10
frame.encode_8b10b(alpide_frame=None, num_leading_comma=num_leading_comma, num_trailing_comma=num_trailing_comma)

frame.write_alpide_format_csv("{}/alpide_format.txt".format(testcase), force=True, append=True)
frame.write_8b10b_csv("{0}/{0}_alpideformat_8b10b.txt".format(testcase), force=True, append=True)

# Manual Data
frame = Alpide_Frame_Generator()
bunch_cnt = random.randint(0, 0xFF)
frame.alpide_frame.append(frame.header(chipid, bunch_cnt))
frame.alpide_frame.extend(frame.random_idle_comma(True, True, False))
frame.alpide_frame.append(frame.region_header(5))
frame.alpide_frame.append(frame.data_long(4, 4 , 4))
frame.alpide_frame.extend(frame.random_idle_comma(True, True, False))
frame.alpide_frame.append(frame.region_header(6))
frame.alpide_frame.append(frame.trailer())

num_leading_comma = 10
num_trailing_comma = 10
frame.encode_8b10b(alpide_frame=None, num_leading_comma=num_leading_comma, num_trailing_comma=num_trailing_comma)

frame.write_alpide_format_csv("{}/alpide_format.txt".format(testcase), force=True, append=True)
frame.write_8b10b_csv("{0}/{0}_alpideformat_8b10b.txt".format(testcase), force=True, append=True)

print("Generating pRU data")
frame = PRU_Frame_Generator()
frame.alpide_frame.read_alpide_frame("{}/alpide_format.txt".format(testcase))
frame.encode_multiple_pru_frames(starting_frame_id=0, chipid=chipid, stave=0xF, ru=0x3F, spill_id=0xFFFF)
frame.write_pru_format_csv("{0}/{0}_pruformat.txt".format(testcase), force=True, append=True)


# Test case 17
# Protocol errors

print("Generating testcase 17")
testcase = "testcase17"
shutil.rmtree(testcase, ignore_errors=True)
os.makedirs(testcase, exist_ok=True)

print("Generating ALPIDE data")
chipid = 0x0

# Manual Data
frame = Alpide_Frame_Generator()
bunch_cnt = random.randint(0xF, 0xFF)
frame.alpide_frame.append(frame.header(chipid, bunch_cnt))
frame.alpide_frame.extend(frame.random_idle_comma(True, True, False))
frame.alpide_frame.append(frame.decode_error())
frame.alpide_frame.append(frame.trailer())

num_leading_comma = 300
num_trailing_comma = 10
frame.encode_8b10b(alpide_frame=None, num_leading_comma=num_leading_comma, num_trailing_comma=num_trailing_comma)

frame.write_alpide_format_csv("{}/alpide_format.txt".format(testcase), force=True, append=True)
frame.write_8b10b_csv("{0}/{0}_alpideformat_8b10b.txt".format(testcase), force=True, append=True)

# Manual Data
frame = Alpide_Frame_Generator()
bunch_cnt = random.randint(0xF, 0xFF)
frame.alpide_frame.append(frame.header(chipid, bunch_cnt))
frame.alpide_frame.extend(frame.random_idle_comma(True, True, False))
frame.alpide_frame.append(frame.protocol_error())
frame.alpide_frame.append(frame.trailer())

num_leading_comma = 10
num_trailing_comma = 10
frame.encode_8b10b(alpide_frame=None, num_leading_comma=num_leading_comma, num_trailing_comma=num_trailing_comma)

frame.write_alpide_format_csv("{}/alpide_format.txt".format(testcase), force=True, append=True)
frame.write_8b10b_csv("{0}/{0}_alpideformat_8b10b.txt".format(testcase), force=True, append=True)

# Manual Data
frame = Alpide_Frame_Generator()
bunch_cnt = random.randint(0xF, 0xFF)
frame.alpide_frame.append(frame.header(chipid, bunch_cnt))
frame.alpide_frame.extend(frame.random_idle_comma(True, True, False))
frame.alpide_frame.append(frame.byte(0x40)) # Data short
frame.alpide_frame.append(frame.decode_error())
frame.alpide_frame.append(frame.trailer())

num_leading_comma = 10
num_trailing_comma = 10
frame.encode_8b10b(alpide_frame=None, num_leading_comma=num_leading_comma, num_trailing_comma=num_trailing_comma)

frame.write_alpide_format_csv("{}/alpide_format.txt".format(testcase), force=True, append=True)
frame.write_8b10b_csv("{0}/{0}_alpideformat_8b10b.txt".format(testcase), force=True, append=True)

# Manual Data
frame = Alpide_Frame_Generator()
bunch_cnt = random.randint(0xF, 0xFF)
frame.alpide_frame.append(frame.header(chipid, bunch_cnt))
frame.alpide_frame.extend(frame.random_idle_comma(True, True, False))
frame.alpide_frame.append(frame.byte(0x0)) # Data long
frame.alpide_frame.append(frame.decode_error())
frame.alpide_frame.append(frame.byte(0x0))
frame.alpide_frame.append(frame.trailer())

num_leading_comma = 10
num_trailing_comma = 10
frame.encode_8b10b(alpide_frame=None, num_leading_comma=num_leading_comma, num_trailing_comma=num_trailing_comma)

frame.write_alpide_format_csv("{}/alpide_format.txt".format(testcase), force=True, append=True)
frame.write_8b10b_csv("{0}/{0}_alpideformat_8b10b.txt".format(testcase), force=True, append=True)

# Manual Data
frame = Alpide_Frame_Generator()
bunch_cnt = random.randint(0xF, 0xFF)
frame.alpide_frame.append(frame.header(chipid, bunch_cnt))
frame.alpide_frame.extend(frame.random_idle_comma(True, True, False))
frame.alpide_frame.append(frame.byte(0x0)) # Data long
frame.alpide_frame.append(frame.byte(0x0))
frame.alpide_frame.append(frame.decode_error())
frame.alpide_frame.append(frame.trailer())

num_leading_comma = 10
num_trailing_comma = 10
frame.encode_8b10b(alpide_frame=None, num_leading_comma=num_leading_comma, num_trailing_comma=num_trailing_comma)

frame.write_alpide_format_csv("{}/alpide_format.txt".format(testcase), force=True, append=True)
frame.write_8b10b_csv("{0}/{0}_alpideformat_8b10b.txt".format(testcase), force=True, append=True)

# Manual Data
frame = Alpide_Frame_Generator()
bunch_cnt = random.randint(0xF, 0xFF)
frame.alpide_frame.append(frame.byte(0xA0)) # Header
frame.alpide_frame.append(frame.decode_error())
frame.alpide_frame.append(frame.trailer())

num_leading_comma = 10
num_trailing_comma = 10
frame.encode_8b10b(alpide_frame=None, num_leading_comma=num_leading_comma, num_trailing_comma=num_trailing_comma)

frame.write_alpide_format_csv("{}/alpide_format.txt".format(testcase), force=True, append=True)
frame.write_8b10b_csv("{0}/{0}_alpideformat_8b10b.txt".format(testcase), force=True, append=True)


print("Generating pRU data")
frame = PRU_Frame_Generator()
frame.alpide_frame.read_alpide_frame("{}/alpide_format.txt".format(testcase))
frame.encode_multiple_pru_frames(starting_frame_id=0, chipid=chipid, stave=0, ru=0, spill_id=0)
frame.write_pru_format_csv("{0}/{0}_pruformat.txt".format(testcase), force=True, append=True)

# Test case 18
# Buffer Overflow

print("Generating testcase 18")
testcase = "testcase18"
shutil.rmtree(testcase, ignore_errors=True)
os.makedirs(testcase, exist_ok=True)

first_iter = True
print("Generating ALPIDE data")
for i in range(1, 3):
    data_gen = Alpide_Data_Generator()
    data = data_gen.random_data(5)
    data_gen.write_row_col_csv(data, "{}/row_col_{}.txt".format(testcase, i), force=True)

    frame = Alpide_Frame_Generator()
    frame.read_row_col_list("{}/row_col_{}.txt".format(testcase, i))
    frame.encode_reg_enc_addr()
    bunch_cnt = random.randint(0, 0xFF)
    frame.encode_alpide_df(add_random_comma=False, add_random_idle=False, add_random_busy=False, clustering_enable=True, bunch_cnt=bunch_cnt, chipid=0)
    if first_iter:
        num_leading_comma = 300
    else:
        num_leading_comma = 1
    num_trailing_comma = random.randint(5, 10)
    frame.encode_8b10b(alpide_frame=None, num_leading_comma=num_leading_comma, num_trailing_comma=num_trailing_comma)

    frame.write_alpide_format_csv("{}/alpide_format_{}.txt".format(testcase, i), force=True)
    frame.write_8b10b_csv("{0}/{0}_alpideformat_8b10b.txt".format(testcase), force=True, append=True)
    first_iter = False

print("Generating pRU data")
for i in range(1, 3):
    frame = PRU_Frame_Generator()
    frame.alpide_frame.read_alpide_frame("{}/alpide_format_{}.txt".format(testcase, i))
    frame.encode_pru_frame(frame_id=i-1, buffer_overflow_error=1)
    frame.write_pru_format_csv("{0}/{0}_pruformat.txt".format(testcase), force=True, append=True)