import csv
import os
from collections import namedtuple
from alpide_data import Alpide_Frame_Generator

LayerStaveChip=namedtuple("LayerStaveChip","layer stave chip frameid")

class PRU_Frame_Generator(object):
    
    def __init__(self, alpide_data_frame=None):
        self.pru_frame = list()
        self.consec_empty_frames = 0

        if alpide_data_frame is not None:
            self.alpide_frame = alpide_data_frame
            self.encode_pru_frame(self.alpide_frame)
        else:
            self.alpide_frame = Alpide_Frame_Generator()

    def encode_pru_frame(self, alpide_frame=None, ru=0, stave=0, chipid=0, busy_status=False, data_format=2, busy_on=0, busy_off=0, spill_id=0, trig_source=0, mode=0, frame_id=0, abs_time=0, 
                         filter_idle=True, filter_comma=True, max_size=0x100000, max_wait_time=0x258, buffer_overflow_error=0, double_busy_on_error=0, double_busy_off_error=0, append=False, verbose=False):
        if verbose:
            print("Encoding pRU Frame")
        frame = list()
        if alpide_frame is None:
            alpide_frame = self.alpide_frame.alpide_frame
        # pRU Header
        frame.append(["header", self.pru_header(self.ru, self.stave, self.chipid, data_format, busy_on, busy_off, spill_id, trig_source, mode, self.frame_id, self.abs_time)])
        current_word = list()
        next_word = list()
        after_header = False
        after_trailer = False

        empty_region_error = 0
        decode_protocol_error = 0
        max_size_error = 0
        max_wait_time_error = 0
        frame_error = 0
        max_size_cnt = 0
        max_wait_time_cnt = 0

        empty_region_test = False

        busy = busy_status

        for i, word in enumerate(alpide_frame):
            if max_size != 0 and max_size_cnt >= max_size:
                max_size_error = 1
                frame_error = 1
                break
            elif max_wait_time != 0 and max_wait_time_cnt >= max_wait_time:
                max_wait_time_error = 1
                frame_error = 1
                break
            
            bytes_int = int(word[1], 16)

            if word[0] == 'busy_on':
                if busy and after_header:
                    double_busy_on_error = 1
                    decode_protocol_error = 1
                busy = True
            
            if word[0] == 'busy_off':
                if not busy and after_header:
                    double_busy_off_error = 1
                    decode_protocol_error = 1
                busy = False

            if word[0] in ['decode_error', 'protocol_error'] and after_header:
                decode_protocol_error = 1
                max_size_cnt += 1
                current_word.append(bytes_int)

            if word[0] in ["trailer", "region_header", "busy_on", "busy_off", "byte"]:

                if word[0] == "region_header":
                    if empty_region_test:
                        empty_region_error = 1
                        decode_protocol_error = 1
                    empty_region_test = True
                
                max_wait_time_cnt = 0
                if not after_header and word[0] in ["busy_on", "busy_off"]:
                    # Do not add busy on and busy off before a header
                    pass
                else:
                    max_size_cnt += 1
                    current_word.append(bytes_int)
                if word[0] == 'trailer':
                    after_trailer = True
            
            elif word[0] in ["idle"] and filter_idle is False and after_trailer is False and after_header is True:
                max_size_cnt += 1
                max_wait_time_cnt += 1
                current_word.append(bytes_int)
            
            elif word[0] in ['comma'] and filter_comma is False and after_trailer is False and after_header is True:
                max_size_cnt += 1
                max_wait_time_cnt += 1
                current_word.append(bytes_int)

            # Just for wait time count
            elif word[0] in ["idle"] and filter_idle is True and after_trailer is False and after_header is True:
                max_wait_time_cnt += 1
            
            elif word[0] in ['comma'] and filter_comma is True and after_trailer is False and after_header is True:
                max_wait_time_cnt += 1
            
            elif word[0] in ["header", "data_short", "empty_frame"]:

                if word[0] == "data_short":
                    empty_region_test = False

                max_wait_time_cnt = 0
                max_size_cnt += 2

                if word[0] == 'header':
                    after_header = True

                # Check if all bytes fits in current word
                if len(current_word) <= 12:
                    byte = (bytes_int >> 8) & 0xFF
                    current_word.append(byte)
                    byte = bytes_int & 0xFF
                    current_word.append(byte)
                elif len(current_word) == 13:
                    byte = (bytes_int >> 8) & 0xFF
                    current_word.append(byte)
                    byte = bytes_int & 0xFF
                    next_word.append(byte)
                else:
                    raise Exception("Something horrible has happened!")
            
            elif word[0] in ["data_long"]: # 3 byte words

                empty_region_test = False

                max_wait_time_cnt = 0
                max_size_cnt += 3

                # Check if all bytes fits in current word
                if len(current_word) <= 11:
                    byte = (bytes_int >> 16) & 0xFF
                    current_word.append(byte)
                    byte = (bytes_int >> 8) & 0xFF
                    current_word.append(byte)
                    byte = bytes_int & 0xFF
                    current_word.append(byte)
                elif len(current_word) == 12:
                    byte = (bytes_int >> 16) & 0xFF
                    current_word.append(byte)
                    byte = (bytes_int >> 8) & 0xFF
                    current_word.append(byte)
                    byte = bytes_int & 0xFF
                    next_word.append(byte)

                elif len(current_word) == 13:
                    byte = (bytes_int >> 16) & 0xFF
                    current_word.append(byte)
                    byte = (bytes_int >> 8) & 0xFF
                    next_word.append(byte)
                    byte = bytes_int & 0xFF
                    next_word.append(byte)
                    
                else:
                    print(current_word)
                    raise Exception("Something horrible has happened!")

            # Check sizes
            if len(current_word) == 14:
                # Create pRU Data Word
                data_word = self.combine_bytes(current_word)
                frame.append(["data", self.pru_word(0x0, ru, stave, chipid, data_word)])

                # Assign next_word to current_word
                current_word = list(next_word)
                next_word = list()

        # If last iteration and current_word is not empty we must add a last pRU word
        if current_word:
            data_word = self.combine_bytes(current_word)
            frame.append(["data", self.pru_word(0x0, ru, stave, chipid, data_word)])
        
        # Add pRU Trailer
        if empty_region_test and not(max_size_error == 1 or max_wait_time_error == 1):
            empty_region_error = 1
            decode_protocol_error = 1
        
        frame_id=self.frame_id
        frame.append(["trailer", self.pru_trailer(self.ru, self.stave, self.chipid, max_size_error=max_size_error, max_wait_time_error=max_wait_time_error, double_busy_on_error=double_busy_on_error,
                                                  buffer_overflow_error=buffer_overflow_error, double_busy_off_error=double_busy_off_error, empty_region_error=empty_region_error, decode_protocol_error=decode_protocol_error, frame_error=frame_error, frame_id=frame_id, frame_size=max_size_cnt)])

        if append:
            self.pru_frame.extend(list(frame))
        else:
            self.pru_frame = list(frame)

    def encode_multiple_pru_frames(self, ru=0, stave=0, chipid=0, data_format=2, busy_on=0, busy_off=0, spill_id=0, trig_source=0, mode=0, starting_frame_id=0, 
                                   abs_time=0, filter_idle=True, filter_comma=True, empty_frame_compression=0xFFFF, check_id=True, verbose=False):
        if verbose:
            print("Encoding multiple pRU Frames")
        
        frames = list()
        alpide_frames = self.alpide_frame.alpide_frame

        last = 0
        start = 0
        # Create lists of frames
        for i, word in enumerate(alpide_frames):
            if word[0] == 'trailer':
                last = i+1
                frame = alpide_frames[start:last]
                frames.append(frame)
                start = i+1
            elif word[0] == 'empty_frame':
                last = i+1
                frame = alpide_frames[start:last]
                frames.append(frame)
                start = i+1


        frame_id = starting_frame_id
        busy = False
        for frame in frames:
            busy_on_indicator = 0
            busy_off_indicator = 0

            if not self.check_id(frame, chipid):
                continue

            if self.busy_on_before_header(frame):
                busy_on_indicator = 1
            
            if self.busy_off_before_header(frame):
                busy_off_indicator = 1

            if self.is_empty_frame(frame):
                self.consec_empty_frames += 1

                # Store values for first instance
                if self.consec_empty_frames == 1:
                    for word in frame:
                        if word[0] == 'empty_frame':
                            empty_bunch_cnt = int(word[1], 16) & 0xFF
                    
                    empty_frame_id = frame_id

                if self.consec_empty_frames == empty_frame_compression:
                    self.pru_frame.append(["empty", self.pru_empty_word(ru, stave, chipid, self.consec_empty_frames, empty_bunch_cnt, spill_id, trig_source, mode, empty_frame_id, abs_time)])
                    self.consec_empty_frames = 0

            elif self.is_reg_frame(frame):
                self.consec_empty_frames = 0
                self.encode_pru_frame(frame, ru, stave, chipid, busy_status=busy, spill_id=spill_id, trig_source=trig_source, mode=mode, busy_on=busy_on_indicator, busy_off=busy_off_indicator,
                                      abs_time=abs_time, frame_id=frame_id, append=True)

                
            frame_id += 1
            busy = self.get_last_busy_status(frame, busy)

    def check_id(self, lst, chipid):
        for word in lst:
            if word[0] == 'header' or word[0] == 'empty_frame':
                if (int(word[1], 16) >> 8) & 0xF != chipid:
                    #print("What? {}".format((int(word[1], 16) >> 8) & 0xF))
                    return False
                else:
                    return True
            elif word[0] == 'byte' and (int(word[1], 16) >> 4) & 0xF == 0xA:
                if (int(word[1], 16) & 0xF) != chipid:
                    return False
                else:
                    return True
        print("Did not find any header!")
        print(lst)
        exit()
        return False

    def busy_on_before_header(self, lst):
        for word in lst:
            if word[0] == 'busy_on':
                return True
            if word[0] == 'header' or word[0] == 'empty_frame':
                return False
    
    def busy_off_before_header(self, lst):
        for word in lst:
            if word[0] == 'busy_off':
                return True
            if word[0] == 'header' or word[0] == 'empty_frame':
                return False
    
    def get_last_busy_status(self, lst, init_busy_status=False):
        busy = init_busy_status
        for word in lst:
            if word[0] == 'busy_off':
                busy = False
            if word[0] == 'busy_on':
                busy = True
        return busy
    
    def is_reg_frame(self, lst):
        for word in lst:
            if word[0] == 'header':
                return True
        return False

    def is_empty_frame(self, lst):
        for word in lst:
            if word[0] == 'empty_frame':
                return True
        return False

    def combine_bytes(self, data_list):
        word = 0
        if len(data_list) < 14:
            # Padding
            for _ in range(14-len(data_list)):
                data_list.append(0xFF)
            
        for i, byte in enumerate(reversed(data_list)):
            word += byte << (i*8)
        return word

    def pru_word(self, word_type, ru, stave, chipid, content):
        word = int("{:02b}{:06b}{:04b}{:04b}{:0112b}".format(word_type, self.ru, self.stave, self.chipid, content), 2)
        return word

    def pru_header(self, ru, stave, chipid, data_format, busy_on, busy_off, spill_id, trig_source, mode, frame_id, abs_time):
        if abs_time > 2**32:
            abs_time=abs_time%2**32
            
        content = int("{:08b}{:019b}{:01b}{:01b}{:016b}{:02b}{:01b}{:032b}{:032b}".format(data_format, 0, busy_on, busy_off, spill_id, trig_source, mode, self.frame_id, self.abs_time), 2)
        word = self.pru_word(0b1, self.ru, self.stave, self.chipid, content)
        return word

    def pru_trailer(self, ru, stave, chipid, max_wait_time_error=0, max_size_error=0, buffer_overflow_error=0, double_busy_off_error=0, double_busy_on_error=0, 
                    empty_region_error=0, frame_error=0, decode_protocol_error=0, frame_id=1, frame_size=0):
        content = int("{:01b}{:01b}{:01b}{:01b}{:01b}{:01b}{:01b}{:01b}{:032b}{:032b}".format(max_wait_time_error, max_size_error, buffer_overflow_error, double_busy_off_error, 
                                                                                       double_busy_on_error, empty_region_error, frame_error, decode_protocol_error, frame_id, frame_size), 2)
        word = self.pru_word(0x2, self.ru, self.stave, self.chipid, content)
        return word
        
    def pru_empty_word(self, ru, stave, chipid, num_empty, bunch_cnt, spill_id, trig_source, mode, frame_id, abs_time):
        reserved = 0x0
        unused = 0x0
        content = int("{:01b}{:04b}{:016b}{:08b}{:016b}{:02b}{:01b}{:032b}{:032b}".format(reserved, unused, num_empty, bunch_cnt, spill_id, trig_source, mode, self.frame_id, self.abs_time), 2)
        word = self.pru_word(0x3, self.ru, self.stave, self.chipid, content)
        return word

    def pru_delimiter(self):
        raise NotImplementedError()

    def write_pru_format_csv(self, path, force=False, append=False, verbose=False):
        if verbose:
            print("Writing to {}".format(path))
        if append:
            file_mode = 'a+'
        else:
            file_mode = 'w+'
        if(os.path.isfile(path) and force is False and input("File exists! Overwrite? (y/N): ").upper() != "Y"):
            print("Aborting...")
            exit(1)
        else:
            with open(path, file_mode, newline='\n') as csvfile:
                csvwriter = csv.writer(
                    csvfile, delimiter=' ', quotechar='|', quoting=csv.QUOTE_MINIMAL)
                for word in self.pru_frame:
                        csvwriter.writerow(["0x{:032X}".format(word[1])])
    

if __name__ == "__main__":
    frame = PRU_Frame_Generator()
    frame.alpide_frame.read_alpide_frame("alpide_format.txt")
    frame.encode_pru_frame(frame.alpide_frame, 0, 0, 0)
    frame.write_pru_format_csv("pru_format.txt", force=True)