from alpide_data import *
from pru_data import *
import os
import shutil
import glob
import re

#Test case data_chain
#Purpose: To convert geant4 simulation into pRU data format
#Output: data_chain_pruformat.txt file in dat folder.

testcase = "data_chain"
shutil.rmtree(testcase, ignore_errors=True)
os.makedirs(testcase, exist_ok=True)

os.listdir('.')
rowcols=glob.glob('dat/row_col*')
print("Generating ALPIDE data")
for i in rowcols:
    # Split filename to get more info
    split_file = re.split(r'[_.]\s*', i)
    layer = int(split_file[2])
    stave = int(split_file[3])
    chipid = int(split_file[4])
    bunch_cnt = int(split_file[5])

    frame=Alpide_Frame_Generator()
    frame.read_row_col_list(i)
    frame.encode_reg_enc_addr()
    frame.chipid = chipid
    frame.bunch_cnt = bunch_cnt
    
    #Quickfix, change increment of chip id in readPctFiles.C
    if chipid ==-1:
        frame.chipid = 0

    frame.encode_alpide_df(reg_enc_addr_list=None, add_random_comma=False, add_random_idle=False, clustering_enable=True)
    frame.write_alpide_format_csv("{}/alpide_format_{}_{}_{}_{}.txt".format(testcase,layer,stave,chipid,bunch_cnt),force=True)

print("Generating pRU data")

for i in rowcols:
    frame =PRU_Frame_Generator()
    # Split filename to get more info
    split_file = re.split(r'[_.]\s*', i)
    ru = int(split_file[2])
    stave = int(split_file[3])
    chipid = int(split_file[4])
    bunch_cnt = int(split_file[5]) 
    frame_id= int(bunch_cnt/400)
    abs_time =int(bunch_cnt*3)
    
    
    frame.alpide_frame.read_alpide_frame("{}/alpide_format_{}_{}_{}_{}.txt".format(testcase,ru,stave,chipid,bunch_cnt))
    frame.frame_id=frame_id
    frame.chipid = chipid
    frame.stave=stave
    frame.ru=ru
    frame.bunch_cnt = bunch_cnt
    frame.abs_time = abs_time
    
    #Quickfix, change increment of chip id in readPctFiles.C
    if chipid ==-1:
        frame.chipid=0

    
    frame.encode_pru_frame()
    frame.write_pru_format_csv("{0}/{0}_pruformat.txt".format(testcase), force=True, append=True)
 
