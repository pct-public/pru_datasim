import csv
import os.path
from collections import namedtuple
from operator import attrgetter
import random
import numpy
from encdec_8b10b import EncDec_8B10B as EncDec

RowCol = namedtuple("RowCol", "row col")
PosTime = namedtuple("PosTime", "bunch_cnt chip")

class Alpide_Data_Generator(object):
    """ Class for generating deterministic x-y pixel hits """

    def __init__(self):
        pass

    def static_data(self, starting_row=0, ending_row=511, starting_col=0, ending_col=1023, sel_occupancy=100, verbose=False):
        """ Generate a given number of hits based on occupancy for the selected row and column range """
        row_col_list = list()
        if sel_occupancy != 100:
            # Currently only support a 100% occupancy for the selected rows and columns
            raise NotImplementedError()

        # How many pixel hits will be generated?
        pixel_hits = int((ending_row+1-starting_row) *
                         (ending_col+1-starting_col)*(sel_occupancy/100))
        if verbose:
            print("Generating {:d} pixel hits...".format(pixel_hits))

        for row in range(starting_row, ending_row+1):
            for col in range(starting_col, ending_col+1):
                row_col_list.append(RowCol(row, col))

        if verbose:
            print("Hit size: {}".format(len(row_col_list)))
        return row_col_list

    def random_data(self, occupancy, verbose=False):
        row_col_list = list()
        for row in range(512):
            for col in range(1024):
                if numpy.random.rand()*100 < occupancy:
                    row_col_list.append(RowCol(row, col))
        if verbose:
            print("Hit size: {}".format(len(row_col_list)))
        return row_col_list

    def write_row_col_csv(self, data, path, force=False, verbose=False):
        if verbose:
            print("Writing to {}".format(path))
        if(os.path.isfile(path) and force is False and input("File exists! Overwrite? (y/N): ").upper() != "Y"):
            print("Aborting...")
            exit(1)
        else:
            with open(path, 'w', newline='\n') as csvfile:
                csvwriter = csv.writer(
                    csvfile, delimiter=' ', quotechar='|', quoting=csv.QUOTE_MINIMAL)
                for hit in data:
                    csvwriter.writerow([hit.row, hit.col])


class Alpide_Frame_Generator(object):
    """ Class for generating an ALPIDE frame with deterministic output for simulation of firmware """

    def __init__(self, row_col_file=None):
        """ Initialize the ALPIDE Frame Generator. The user needs to specify a path to a CSV row-col file. """
        self.row_col_list = list()
        self.reg_enc_addr_list = list()
        self.alpide_frame = list()
        self.encoded_frame = list()
        
        self.chipid = 0
        self.bunch_cnt = 0

        if row_col_file is not None:

            self.read_position_time_list(position_time_file)
            self.read_row_col_list(row_col_file)
            self.encode_pos_time(self.position_time_list)
            self.encode_reg_enc_addr(self.row_col_list)
            self.encode_alpide_df(self.reg_enc_addr_list)
            self.encode_8b10b(self.alpide_frame)


    def read_row_col_list(self, row_col_file, verbose=False):
        if verbose:
            print("Reading row_col list")

        if os.path.isfile(row_col_file) is False:
            print("{} does not exist! Aborting...".format(row_col_file))
            #exit(1)
        else:
            with open(row_col_file, 'r', newline='\n') as csvfile:
                csvreader = csv.reader(
                    csvfile, delimiter=' ', quotechar='|', quoting=csv.QUOTE_MINIMAL)
                for line in csvreader:
                    row = line[0]
                    col = line[1]
                    self.row_col_list.append(RowCol(int(row), int(col)))


    def encode_reg_enc_addr(self, row_col_list=None, verbose=False):
        """ Encodes the row-col list into a list of tuples with region, encoder and address 
        """
        if verbose:
            print("Encode reg enc addr")
        if row_col_list is None:
            row_col_list = self.row_col_list
        RegEncAddr = namedtuple('RegEncAddr', 'region encoder_id address')
        for hit in row_col_list:
            region = int(hit.col / 32)
            x_in_region = hit.col - region*32
            encoder_id = int(x_in_region // 2)
            x_in_dcol = int(x_in_region % 2)
            # 4 cases in double col
            if (hit.row % 2 == 0 and x_in_dcol == 0):
                address = int(hit.row*2)
            elif (hit.row % 2 == 0 and x_in_dcol == 1):
                address = int(hit.row*2 + 1)
            elif (hit.row % 2 == 1 and x_in_dcol == 0):
                address = int(hit.row*2 + 1)
            else:
                address = int(hit.row*2)

            self.reg_enc_addr_list.append(
                RegEncAddr(region, encoder_id, address))

        self.reg_enc_addr_list = sorted(self.reg_enc_addr_list)

    def get_random_idles(self, max_idle_sequences):
        idles = list()
        number = random.randint(0, max_idle_sequences)
        for _ in range(number):
            idles.append(self.idle())
        return idles

    def get_random_commas(self, max_comma_sequences):
        commas = list()
        number = random.randint(0, max_comma_sequences) * 3 # Always commas divisable by 3
        for _ in range(number):
            commas.append(self.comma())
        return commas

    def random_idle_comma(self, random_idle, random_comma, random_busy):
        randoms = list()
        if random_busy:
            if random.random() < 0.05:
                randoms.append(self.busy_on())
            if random.random() < 0.05:
                randoms.append(self.busy_off())
        if random_idle:
            if bool(random.getrandbits(1)):
                randoms.extend(self.get_random_idles(10))
        if random_comma:
            if bool(random.getrandbits(1)):
                randoms.extend(self.get_random_commas(10))
        if random_busy:
            if random.random() < 0.01:
                randoms.append(self.busy_on())
            if random.random() < 0.01:
                randoms.append(self.busy_off())
        return randoms


    def encode_alpide_df(self, reg_enc_addr_list=None, add_random_comma=False, add_random_idle=False, add_random_busy=False, clustering_enable=True, verbose=False, busy_violation=False, flushed_incomplete=False,
                         strobe_extended=False, busy_transition=False, data_overrun=False, fatal_condition=False):
        if verbose:
            print("Encode alpide dataformat")
        if reg_enc_addr_list is None:
            reg_enc_addr_list = self.reg_enc_addr_list
        tmp = list()
        tmp.extend(self.random_idle_comma(add_random_idle, False, add_random_busy))
        tmp.append(self.header(self.chipid, self.bunch_cnt))
        tmp.extend(self.random_idle_comma(add_random_idle, add_random_comma, False))
        for reg in range(32):
            # If busy violation, no pixels are read out
            if busy_violation:
                break
            pixels_in_region = [x for x in reg_enc_addr_list if x.region == reg]
            if pixels_in_region:
                # print("Found " + str(len(pixels_in_region)) +
                #       " pixels in region " + str(reg))
                tmp.append(self.region_header(reg))
                tmp.extend(self.random_idle_comma(add_random_idle, add_random_comma, add_random_busy))

                for enc in range(16):
                    pixels_in_dcol = [x for x in pixels_in_region if x.encoder_id == enc]
                    if pixels_in_dcol:
                        pixels_done = list()

                        for i, pixel in enumerate(pixels_in_dcol):
                            data_long_start = False
                            if pixel.address in pixels_done:
                                continue
                            pixels_done.append(pixel.address)
                            pixmap = ["0"]*7
                            if clustering_enable:
                                for next_pix in pixels_in_dcol[i:]:
                                    delta = (next_pix.address - pixel.address)
                                    if 0 != (delta) < 8:
                                        # Found Data Long
                                        pixels_done.append(next_pix.address)
                                        data_long_start = True
                                        pixmap[delta-1] = "1"

                            if data_long_start:
                                pixmap_int = int("".join(reversed(pixmap)), 2)
                                tmp.append(self.data_long(pixel.encoder_id, pixel.address, pixmap_int))
                                tmp.extend(self.random_idle_comma(add_random_idle, add_random_comma, add_random_busy))
                            else:
                                # Found DATA SHORT
                                tmp.append(self.data_short(pixel.encoder_id, pixel.address))
                                tmp.extend(self.random_idle_comma(add_random_idle, add_random_comma, add_random_busy))

        # If not hits, indicates a busy violation
        if not reg_enc_addr_list:
            tmp.append(self.trailer(busy_violation=True))
        else:
            tmp.append(self.trailer(busy_violation=busy_violation, flushed_incomplete=flushed_incomplete, strobe_extended=strobe_extended, busy_transition=busy_transition, 
                                    data_overrun=data_overrun, fatal_condition=fatal_condition))
        tmp.extend(self.random_idle_comma(add_random_idle, add_random_comma, False))
        self.alpide_frame = tmp
    def encode_8b10b(self, alpide_frame=None, num_leading_comma=300, num_trailing_comma=20, verbose=False):
        """ Encodes an ALPIDE frame into 8B10B format """
        if verbose:
            print("Encode 8b10b")
        lst = list()
        running_disparity = 0
        if alpide_frame is None:
            # Use the instance list
            alpide_frame = self.alpide_frame
        
        # Add leading commas
        for i in range(num_leading_comma):
            running_disparity, enc = EncDec.enc_8b10b(0xBC, running_disparity, 1)
            lst.append("{:03x}".format(enc))

        for word in alpide_frame:
            tmp_list = list()
            if word[0] == 'comma': # Special case were control char must be used
                running_disparity, tmp = EncDec.enc_8b10b(word[1], running_disparity, ctrl=1)
                encoded_str = "{:03x}".format(tmp)
                tmp_list.append(encoded_str)

            elif word[0] == 'decode_error': #Special case - no encoding done :(
                encoded_str = "{:03x}".format(0)
                tmp_list.append(encoded_str)
            elif word[0] == 'protocol_error': #Special case - with encoding
                running_disparity, tmp = EncDec.enc_8b10b(word[1], running_disparity, ctrl=0)
                encoded_str = "{:03x}".format(tmp)
                tmp_list.append(encoded_str)

            elif word[0] in ["idle", "trailer", "region_header", "busy_on", "busy_off", "byte"]:
                tmp_string = ["{:02X}".format(word[1])]

                running_disparity, tmp_list = EncDec.enc_8b10b_list(
                    tmp_string, running_disparity)
            elif word[0] in ["header", "data_short", "empty_frame"]:
                tmp_string = ["{:04X}".format(word[1])]

                running_disparity, tmp_list = EncDec.enc_8b10b_list(
                    tmp_string, running_disparity)
            else:
                tmp_string = ["{:06X}".format(word[1])]

                running_disparity, tmp_list = EncDec.enc_8b10b_list(
                    tmp_string, running_disparity)

            lst.extend(tmp_list)

        # Add trailing commas
        for i in range(num_trailing_comma):
            running_disparity, enc = EncDec.enc_8b10b(0xBC, running_disparity, 1)
            lst.append("{:03x}".format(enc))

        self.encoded_frame = lst

    def gen_frame(self, decoded_list):
        raise NotImplementedError()

    def comma(self):
        return "comma", 0xBC

    def idle(self):
        return "idle", 0xFF

    def header(self, chip_id, bunch_cnt):
        if bunch_cnt > 2**12:
            bunch_cnt = bunch_cnt % (2**12)
        bunch_cnt = (bunch_cnt >> 3) & 0xFF
        return "header", int("A{0:01X}{1:02X}".format(chip_id, bunch_cnt), 16)

    def trailer(self, busy_violation=False, data_overrun=False, fatal_condition=False, flushed_incomplete=False, strobe_extended=False, busy_transition=False):
        if busy_violation:
            val = 0xB8
        elif data_overrun:
            val = 0xBC
        elif fatal_condition:
            val = 0xBE
        else:
            val = 0xB0
            if flushed_incomplete:
                val += 0x4
            if strobe_extended:
                val += 0x2
            if busy_transition:
                val += 0x1
        return "trailer", val

    def empty_frame(self, chip_id, bunch_cnt):
        return "empty_frame", int("E{0:01X}{1:02X}".format(chip_id, bunch_cnt), 16)

    def region_header(self, region_id):
        return "region_header", int("110{:05b}".format(region_id), 2)

    def data_short(self, encoder_id, addr):
        return "data_short", int("01{0:04b}{1:010b}".format(encoder_id, addr), 2)

    def data_long(self, encoder_id, addr, hitmap):
        return "data_long", int("00{0:04b}{1:010b}0{2:07b}".format(encoder_id, addr, hitmap), 2)

    def busy_on(self):
        return "busy_on", 0xF1

    def busy_off(self):
        return "busy_off", 0xF0

    def decode_error(self):
        return "decode_error", 0x5F

    def protocol_error(self):
        return "protocol_error", 0xF3

    def byte(self, byte):
        return "byte", byte

    def read_alpide_frame(self, alpide_data_path, verbose=False):
        if verbose:
            print("Reading Alpide Frame")

        if os.path.isfile(alpide_data_path) is False:
            print("{} does not exist! Aborting...".format(alpide_data_path))
            exit(1)
        else:
            with open(alpide_data_path, 'r', newline='\n') as csvfile:
                csvreader = csv.reader(
                    csvfile, delimiter=' ', quotechar='|', quoting=csv.QUOTE_MINIMAL)
                for line in csvreader:
                    self.alpide_frame.append((line[0], line[1]))

    
    def get_bytes(self, frame=None, filter_idle=True, filter_comma=True):
        num_bytes = 0
        in_frame = False
        if frame is None:
            frame = self.alpide_frame

        for word in frame:
            if word[0] in ["header"]:
                in_frame = True
            elif word[0] in ["trailer"]:
                in_frame = False
            if word[0] in ["trailer", "region_header", "busy_on", "busy_off"]:
                num_bytes += 1
            elif word[0] in ["header", "data_short", "empty_frame"]:
                num_bytes += 2
            elif word[0] in ["data_long"]:
                num_bytes += 3
            elif word[0] in ["idle"] and not filter_idle and in_frame:
                num_bytes += 1
            elif word[0] in ["comma"] and not filter_comma and in_frame:
                num_bytes += 1
        return num_bytes

    def write_reg_enc_addr_csv(self, path, force=False, append=False, verbose=False):
        if verbose:
            print("Writing to {}".format(path))
        if append:
            file_mode = 'a+'
        else:
            file_mode = 'w+'
        if(os.path.isfile(path) and force is False and input("File exists! Overwrite? (y/N): ").upper() != "Y"):
            print("Aborting...")
            exit(1)
        else:
            with open(path, file_mode, newline='\n') as csvfile:
                csvwriter = csv.writer(
                    csvfile, delimiter=' ', quotechar='|', quoting=csv.QUOTE_MINIMAL)
                for hit in self.reg_enc_addr_list:
                    csvwriter.writerow(
                        [hit.region, hit.encoder_id, hit.address])

    def write_alpide_format_csv(self, path, force=False, append=False, verbose=False):
        if verbose:
            print("Writing to {}".format(path))
        if append:
            file_mode = 'a+'
        else:
            file_mode = 'w+'
        if(os.path.isfile(path) and force is False and input("File exists! Overwrite? (y/N): ").upper() != "Y"):
            print("Aborting...")
            exit(1)
        else:
            with open(path, file_mode, newline='\n') as csvfile:
                csvwriter = csv.writer(
                    csvfile, delimiter=' ', quotechar='|', quoting=csv.QUOTE_MINIMAL)
                for word in self.alpide_frame:
                    if word[0] in ["comma", "idle", "trailer", "region_header", "busy_on", "busy_off", "decode_error", "protocol_error", "byte"]:
                        csvwriter.writerow([word[0], "{:02X}".format(word[1])])
                    elif word[0] in ["header", "data_short", "empty_frame"]:
                        csvwriter.writerow([word[0], "{:04X}".format(word[1])])
                    else:
                        csvwriter.writerow([word[0], "{:06X}".format(word[1])])

    def write_8b10b_csv(self, path, force=False, append=False, verbose=False):
        if verbose:
            print("Writing to {}".format(path))
        if append:
            file_mode = 'a+'
        else:
            file_mode = 'w+'
        if(os.path.isfile(path) and force is False and input("File exists! Overwrite? (y/N): ").upper() != "Y"):
            print("Aborting...")
            exit(1)
        else:
            with open(path, file_mode, newline='\n') as csvfile:
                csvwriter = csv.writer(
                    csvfile, delimiter=' ', quotechar='|', quoting=csv.QUOTE_MINIMAL)
                for word in self.encoded_frame:
                    csvwriter.writerow([word])


def write_single_row():
    data_gen = Alpide_Data_Generator()
    data = data_gen.static_data(0, 0, 0, 5)
    data_gen.write_row_col_csv(data, "row_col.txt", force=True)

def write_random_data():
    data_gen = Alpide_Data_Generator()
    data = data_gen.random_data(0.001)
    data_gen.write_row_col_csv(data, "row_col.txt", force=True)


def read_row_col_file():
    frame = Alpide_Frame_Generator("row_col.txt")
    
    print(frame.row_col_list)
    print(frame.reg_enc_addr_list)
    print(frame.alpide_frame)
    print(frame.encoded_frame)
    frame.write_reg_enc_addr_csv("reg_enc_addr.txt", force=True)
    frame.write_alpide_format_csv("alpide_format.txt", force=True)
    frame.write_8b10b_csv("8b10b.txt", force=True)

def manual():
    frame = Alpide_Frame_Generator()

    frame.read_row_col_list("row_col.txt")
    frame.encode_reg_enc_addr()
    frame.encode_alpide_df()
    print("Num bytes in frame: {}".format(frame.get_bytes()))
    frame.encode_8b10b()
    #print(frame.row_col_list)
    #print(frame.reg_enc_addr_list)
    #print(frame.alpide_frame)
    #print(frame.encoded_frame)
    frame.write_reg_enc_addr_csv("reg_enc_addr.txt", force=True)
    frame.write_alpide_format_csv("alpide_format.txt", force=True)
    frame.write_8b10b_csv("8b10b.txt", force=True)


if __name__ == "__main__":
    #write_single_row()
    #write_random_data()
    #read_row_col_file()
    manual()
