import os
import shutil
import re

copy_dir = "../../firmware/source/modules/alpide_data/tb"
dirs = os.listdir()
dirs = [x for x in dirs if os.path.isdir(x)]
dirs = [x for x in dirs if re.compile(r"testcase\d+").match(x)]
print("Found the following testcase dirs: ")
print(dirs)

for d in dirs:
    files = os.listdir(d)
    files = [x for x in files if os.path.isfile(os.path.join(d, x))]
    files = [x for x in files if re.compile(r"testcase\d+_(pruformat|alpideformat_8b10b)\.txt").match(x)]
    print("Found the following files: ")
    print(files)

    for f in files:
        copy_file = os.path.join(d, f)
        shutil.copy(copy_file, copy_dir)